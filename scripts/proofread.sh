#!/bin/bash
# Використання: ./scripts/proofread.sh 001.asc

# source setup_variables.sh

# ASC2TXT_OUTPUT_DIR - каталог для виведення файлів
# LANGUAGETOOL_DIR - каталог з LanguageTool
set -e

DIR="$( dirname "$0" )"

if [[ -z "$ASC2TXT_OUTPUT_DIR" ]]; then
  ASC2TXT_OUTPUT_DIR="${DIR}/../tmp"
fi
mkdir -p "$ASC2TXT_OUTPUT_DIR"

FILENAME="${ASC2TXT_OUTPUT_DIR}/$(basename -s .asc "$1").txt"
"$DIR/chapter2text.sh" "$1" > "$FILENAME"

languagetool "$1" || java -jar "${LANGUAGETOOL_DIR}/languagetool.jar" "$FILENAME"
